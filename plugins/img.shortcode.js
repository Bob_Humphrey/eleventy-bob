const outdent = require('outdent')({ newline: ' ' })

module.exports = (folder, fileName, format, alt) => {
  return outdent`
    <picture>
      <source media="(max-width: 350px)"
        srcset="/img/webp/${folder}/300/${fileName}.webp" type="image/webp">
      <source media="(max-width: 475px)"
        srcset="/img/webp/${folder}/400/${fileName}.webp" type="image/webp">
      <source media="(max-width: 1024px)"
        srcset="/img/webp/${folder}/sm/${fileName}.webp" type="image/webp">
      <source media="(max-width: 1280px)"
        srcset="/img/webp/${folder}/lg/${fileName}.webp" type="image/webp">
      <source media="(min-width: 1281px)" 
        srcset="/img/webp/${folder}/xl/${fileName}.webp" type="image/webp">

      <source media="(max-width: 350px)"
        srcset="/img/resized/${folder}/300/${fileName}.${format}">
      <source media="(max-width: 475px)"
        srcset="/img/resized/${folder}/400/${fileName}.${format}">
      <source media="(max-width: 1024px)"
        srcset="/img/resized/${folder}/sm/${fileName}.${format}">
      <source media="(max-width: 1280px)"
        srcset="/img/resized/${folder}/lg/${fileName}.${format}">
      <source media="(max-width: 1281px)"
        srcset="/img/resized/${folder}/xl/${fileName}.${format}">
      <img src="/img/original/${folder}/${fileName}.${format}" loading="lazy" alt="${alt}">
    </picture>`
}