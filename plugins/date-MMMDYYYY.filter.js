
const moment = require('moment')

moment.locale('en')

module.exports = (date) => {
  return moment(date).utc().format("MMM D YYYY") 
}