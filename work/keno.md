---
title: "Keno"
layout: work.njk
date: "2020-02-23"
img: "keno"
tags: work
categories: ["javascript", "react", "redux", "tailwind css"]
url: "https://keno.bob-humphrey.com"
code_url: "https://gitlab.com/Bob_Humphrey/react-keno"
additional_code_url: ""
learn_more_url: ""
---

React version of a popular casino game.
