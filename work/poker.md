---
title: "Poker"
layout: work.njk
date: "2020-03-02"
img: "poker"
tags: work
categories: ["javascript", "react", "react context", "tailwind css"]
url: "https://poker.bob-humphrey.com"
code_url: "https://gitlab.com/Bob_Humphrey/react-poker"
additional_code_url: ""
learn_more_url: ""
---

Jacks or Better video poker game implemented in React.
