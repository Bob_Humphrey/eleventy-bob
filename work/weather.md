---
title: "Weather Forecast"
layout: work.njk
date: "2020-02-04"
img: "weather"
tags: work
categories:
  [
    darsky api,
    javascript,
    lumen,
    opencage api,
    php,
    react,
    responsive,
    tailwind css,
  ]
url: "https://weather.bob-humphrey.com"
code_url: "https://gitlab.com/Bob_Humphrey/react-weather"
additional_code_url: ""
learn_more_url: ""
---

Displays the current weather and a six day forecast for any city in the world, using the DarkSky weather API and the OpenCage geocoder API.
