---
title: "Color Tool"
layout: work.njk
date: "2020-02-16"
img: "color"
tags: work
categories: ["gatsby", "react", "tailwind css"]
url: "https://color.bob-humphrey.com"
code_url: "https://gitlab.com/Bob_Humphrey/gatsby-scheme"
additional_code_url: ""
learn_more_url: ""
---

A simple tool for working with website color. Create a color scheme and see the results in ten typical web layouts.
