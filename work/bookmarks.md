---
title: "Bookmarks"
layout: work.njk
date: "2020-04-11"
img: "bookmarks"
tags: work
categories:
  ["javascript", "laravel", "mysql", "php", "responsive", "tailwind css"]
url: "https://bookmarks.bob-humphrey.com/"
code_url: "https://gitlab.com/Bob_Humphrey/laravel-bookmarks"
additional_code_url: ""
learn_more_url: ""
---

I created this application to help manage my collection of web development bookmarks. I have a bookmarklet in my browser that runs a little javascript whenever I click it. It takes the title and url of the web page I'm currently viewing and sends it to a Laravel backend. I can then select a category for the bookmark and hit the Save button. The Laravel app lets me create new categories and display my bookmarks by category.
