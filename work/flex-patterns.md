---
title: "Tailwind CSS Patterns"
layout: work.njk
date: "2019-11-05"
img: "tailwind-patterns"
tags: work
categories: ["eleventy", "responsive", "tailwind css"]
url: "https://flex.bob-humphrey.com"
code_url: "https://gitlab.com/Bob_Humphrey/react-flex"
additional_code_url: ""
learn_more_url: ""
---

A reference source and cheat sheet for styling web pages using Tailwind CSS.
Once I've figured out the rules for styling a common layout component,
I like to keep a record of it handy so that I don't have to spend time
doing it again.
