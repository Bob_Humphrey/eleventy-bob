---
title: "Inspiration"
layout: work.njk
date: "2019-11-05"
img: "inspiration"
tags: work
categories: ["laravel", "mysql", "php", "responsive", "tailwind css"]
url: "https://inspiration.bob-humphrey.com/items"
code_url: "https://gitlab.com/Bob_Humphrey/inspiration-laravel"
additional_code_url: ""
learn_more_url: ""
---

An application for adding and organizing a collection of web links related to some of my interests and hobbies.
