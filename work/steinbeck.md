---
title: "John Steinbeck"
layout: work.njk
date: "2020-07-02"
img: "steinbeck"
tags: work
categories:
  [
    "eleventy",
    "javascript",
    "responsive",
    "tailwind css",
  ]
url: "https://steinbeck.bob-humphrey.com/"
code_url: "https://gitlab.com/Bob_Humphrey/eleventy-steinbeck"
additional_code_url: ""
learn_more_url: ""
---

Information about the life and work of Nobel prize winning novelist John Steinbeck.
