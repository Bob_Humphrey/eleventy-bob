---
title: "Text Tool"
layout: work.njk
date: "2020-03-29"
img: "text-tool"
tags: work
categories: ["gatsby", "javascript", "react", "tailwind css"]
url: "https://text.bob-humphrey.com/"
code_url: "https://gitlab.com/Bob_Humphrey/gatsby-fonts"
additional_code_url: ""
learn_more_url: ""
---

This tool allows me to format several pages of text and easily see the effect of changing various properties, including font, size, color, line height and paragraph width.
