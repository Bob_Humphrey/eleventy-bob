---
title: "Personality Test"
layout: work.njk
date: "2020-01-25"
img: "personality"
tags: work
categories:
  ["javascript", "react", "react context", "react-vis charts", "tailwind css"]
url: "https://personality.bob-humphrey.com"
code_url: "https://gitlab.com/Bob_Humphrey/react-personality"
additional_code_url: ""
learn_more_url: ""
---

An interactive test for determining personality type, based on the types created by Katharine Cook Briggs and Isabel Briggs Myers.
