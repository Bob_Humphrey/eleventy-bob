---
title: "Covid-19 Dashboard"
layout: work.njk
date: "2020-06-01"
img: "covid"
tags: work
categories:
  [
    "alpine js",
    "apex charts",
    "api",
    "laravel",
    "mysql",
    "php",
    "responsive",
    "tailwind css",
  ]
url: "https://covid19.bob-humphrey.com/"
code_url: "https://gitlab.com/Bob_Humphrey/laravel-covid19"
additional_code_url: ""
learn_more_url: "https://bob-humphrey.com/articles/covid19/part-1"
---

Built with data from the COVID-Project, this application uses a number of different measures to track the progress of the disease throughout the United States and show where the epidemic is getting better and where it is getting worse.
