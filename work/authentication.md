---
title: "Authentication"
layout: work.njk
date: "2020-04-03"
img: "authentication"
tags: work
categories:
  [
    "javascript",
    "laravel",
    "laravel sanctum",
    "mysql",
    "php",
    "react",
    "react context",
    "responsive",
    "tailwind css",
  ]
url: "https://auth.bob-humphrey.com/"
code_url: "https://gitlab.com/Bob_Humphrey/react-auth"
additional_code_url: "https://gitlab.com/Bob_Humphrey/laravel-auth"
learn_more_url: "https://bob-humphrey.com/articles/sanctum-react/part-1"
---

Laravel Sanctum authentication for a React application.
