
const environment = process.env.ELEVENTY_ENV
const PROD_ENV = 'production'
const prodUrl = 'https://bob-humphrey.com'
const devUrl = 'http://localhost:8080'
const baseUrl = environment === PROD_ENV ? prodUrl : devUrl
const isProd = environment === PROD_ENV
const currentYear = new Date().getFullYear()

module.exports = {
  title: 'Bob Humphrey - Web Developer',
  author: 'Bob Humphrey',
  description: 'Bob Humphrey is a web developer with many years of '
    + ' experience. This site includes examples of his recent work,'
    + ' a list of his skills, and a collection'
    + ' of articles he has written.',
  environment,
  isProd,
  baseUrl,
  currentYear
}