---
id: 1
title: "Bento Box Search"
layout: project.njk 
img: "bento"
path: "/projects/bento/"
tags: project 
---


An application to help students search for library resources, such as books, journals, magazines, and digital collections. Before this was built, they had to enter a separate search for each category of items. 