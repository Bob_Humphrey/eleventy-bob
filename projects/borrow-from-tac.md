---
id: 32
title: "Computers Available for Borrowing"
layout: project.njk 
img: "borrow-from-tac"
path: "/projects/borrow-from-tac/"
tags: project 
---


The library's Technology Assistance Center provides a number of laptops that can be borrowed by students. This application creates a public display that shows the number of laptops currently available. 