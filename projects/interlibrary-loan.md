---
id: 9
title: "Improved Interlibrary Loan"
layout: project.njk 
img: "interlibrary-loan"
path: "/projects/interlibrary-loan/"
tags: project 
---


Allows library patrons to request materials from other libraries through Randall Library. This was vendor software that I customized and improved. 