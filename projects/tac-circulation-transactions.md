---
id: 15
title: "TAC Circulation Transactions"
layout: project.njk 
img: "tac-circulation-transactions"
path: "/projects/tac-circulation-transactions/"
tags: project 
---


This application tracks student transactions at the university's Technology Assistance Center located within the library. The information is useful in helping management schedule the 30 plus part-time student workers to provide adequate coverage during all the hours the center is open. 