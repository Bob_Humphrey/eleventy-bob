---
id: 17
title: "Project Management"
layout: project.njk 
img: "project-management"
path: "/projects/project-management/"
tags: project 
---


The library information technology department was using the Asana web application for managing various projects. This application was able to access the Asana server and create customized reports by project and employee. 