---
id: 35
title: "Linux Servers"
layout: project.njk 
img: "linux"
path: "/projects/linux/"
tags: project 
---


Set up new Linux servers, installed all software, and migrated applications from the library's Windows servers. 