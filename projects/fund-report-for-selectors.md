---
id: 7
title: "Fund Report for Selectors"
layout: project.njk 
img: "fund-report-for-selectors"
path: "/projects/fund-report-for-selectors/"
tags: project 
---


Each year the library establishes a budget by university department for purchasing new books. This application helps the library manage these budgets and purchases. 