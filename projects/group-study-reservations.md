---
id: 2
title: "Group Study Room Reservations"
layout: project.njk 
img: "group-study-reservations"
path: "/projects/group-study-reservations/"
tags: project 
---


An application that allows students to reserve one of the library's private study rooms. With this software, the students are able to completely manage the use of the the various study rooms with almost no involvement from the library's staff. 