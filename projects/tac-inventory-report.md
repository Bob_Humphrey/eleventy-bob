---
id: 48
title: "TAC Inventory Report"
layout: project.njk 
img: "tac-inventory-report"
path: "/projects/tac-inventory-report/"
tags: project 
---


Helps keep track of the inventory of a wide variety of items in the library technical support department. 