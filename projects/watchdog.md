---
id: 31
title: "Watchdog Reports"
layout: project.njk 
img: "watchdog"
path: "/projects/watchdog/"
tags: project 
---


Improved interface for working with the log files and error messages created by Drupal, the library's content management system. 