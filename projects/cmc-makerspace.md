---
id: 3
title: "CMC Makerspace"
layout: project.njk 
img: "cmc-makerspace"
path: "/projects/cmc-makerspace/"
tags: project 
---


The Curriculum Materials Center contains a collection of PreK-12 instructional resources, including a makerspace with stations for 3D doodling/printing/scanning/modeling, book binding, paper-crafting, stamping, painting, prototyping, and sewing. This application assists students and staff in documenting projects completed in the makerspace using descriptions, photos, and inventories of resources consumed. 