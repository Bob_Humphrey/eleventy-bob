---
id: 44
title: "Get It From Randall Library"
layout: project.njk 
img: "get-it-from-randall"
path: "/projects/get-it-from-randall/"
tags: project 
---


Research tool that can be easily added as a button to a student's browser bookmarks toolbar. 