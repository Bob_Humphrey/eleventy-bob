---
id: 13
title: "Info Lit"
layout: project.njk 
img: "info-lit"
path: "/projects/info-lit/"
tags: project 
---


A collection of tools for teaching and learning information literacy skills. 