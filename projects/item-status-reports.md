---
id: 43
title: "Item Status Reports"
layout: project.njk 
img: "item-status-reports"
path: "/projects/item-status-reports/"
tags: project 
---


An enhancement to the library catalog that is used by the staff to find catalog items by means of various criteria. 