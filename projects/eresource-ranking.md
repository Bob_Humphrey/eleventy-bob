---
id: 36
title: "Database Ranking"
layout: project.njk 
img: "eresource-ranking"
path: "/projects/eresource-ranking/"
tags: project 
---


Enhanced the ability of the librarians to customize certain features in the library's content management system. 