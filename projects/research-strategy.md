---
id: 5
title: "Research Strategy"
layout: project.njk 
img: "research-strategy"
path: "/projects/research-strategy/"
tags: project 
---


The library offers access to dozens of online databases, but using them for research can be a challenge. This application walks the student step-by-step through the process of creating an effective search statement to find relevant information in the databases. 