---
id: 46
title: "Link Translator"
layout: project.njk 
img: "link-translator"
path: "/projects/link-translator/"
tags: project 
---


Tool for updating the library's website links after a vendor's information was changed. 