---
id: 11
title: "Special Collections Patron Registration"
layout: project.njk 
img: "special-collections-patron-registration"
path: "/projects/special-collections-patron-registration/"
tags: project 
---


This is used to gather information and create reports concerning patrons using the library's special collection and items contained within the collection. 