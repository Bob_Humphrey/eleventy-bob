---
id: 45
title: "Touch Kiosk"
layout: project.njk 
img: "touch-kiosk"
path: "/projects/touch-kiosk/"
tags: project 
---


Added book and video search to the library's handy public kiosk. 