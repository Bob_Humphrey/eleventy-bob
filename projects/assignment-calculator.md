---
id: 8
title: "Assignment Calculator"
layout: project.njk 
img: "assignment-calculator"
path: "/projects/assignment-calculator/"
tags: project 
---


This application helps students manage their time and meet deadlines by breaking large projects into smaller steps and budgeting specific amounts of time for each step. This is an open source project that I customized for Randall Library.