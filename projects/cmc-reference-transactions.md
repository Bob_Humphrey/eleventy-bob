---
id: 26
title: "CMC Reference Transactions"
layout: project.njk 
img: "cmc-reference-transactions"
path: "/projects/cmc-reference-transactions/"
tags: project 
---


Documents the various occurrences of customer service provided by the library's Curriculum Materials Center. 