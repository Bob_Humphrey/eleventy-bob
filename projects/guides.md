---
id: 16
title: "Guides"
layout: project.njk 
img: "guides"
path: "/projects/guides/"
tags: project 
---


Guides are created for each area of study at the university. Students can use them to locate important library resources and contact the librarian responsible for that field. 