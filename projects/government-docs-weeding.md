---
id: 6
title: "Government Docs Weeding"
layout: project.njk 
img: "government-docs-weeding"
path: "/projects/government-docs-weeding/"
tags: project 
---


Randall Library participates in the Federal Depository Library Program to make government documents available for research and other uses. Over time, the number of these documents exceeds the space available in the library. This application helps library personnel identify items that can be removed from the collection. 