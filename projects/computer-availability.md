---
id: 30
title: "Computer Availability"
layout: project.njk 
img: "computer-availability"
path: "/projects/computer-availability/"
tags: project 
---


There are over one hundred computers in the library that are available for student use. This application provides detailed reporting on how these computers are actually used, by date, time, device type, machine, and location. This information has a number of uses, including identifying malfunctioning machines and establishing justification for new computers as needed. The library's application was written in house to replace an expensive proprietary system. 