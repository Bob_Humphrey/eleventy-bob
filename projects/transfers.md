---
id: 12
title: "Transfers"
layout: project.njk 
img: "transfers"
path: "/projects/transfers/"
tags: project 
---


Manages the reporting and collection of unpaid patron late fees, lost items, and fines. When the amount due become too large, it gets transferred to the student's university account. At that point, the student cannot register for another semester without paying the fines. 