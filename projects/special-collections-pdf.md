---
id: 33
title: "Oral History PDFs"
layout: project.njk 
img: "special-collections-pdf"
path: "/projects/special-collections-pdf/"
tags: project 
---


The library has created several dozen oral histories based on interviews with various members of the Wilmington NC community. These interviews were already available online, but I created a function by which any history could be turned into a PDF document. 