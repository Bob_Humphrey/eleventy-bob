---
id: 19
title: "Instructions"
layout: project.njk 
img: "instructions-statistics"
path: "/projects/instructions-statistics/"
tags: project 
---


Documents all of the various classes taught by library faculty. 