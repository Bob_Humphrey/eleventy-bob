---
id: 34
title: "Click Heat"
layout: project.njk 
img: "clickheat"
path: "/projects/clickheat/"
tags: project 
---


Customized an open source application that creates a visual record of how the library's information kiosk is being used. 