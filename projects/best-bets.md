---
id: 28
title: "Best Bets"
layout: project.njk 
img: "best-bets"
path: "/projects/best-bets/"
tags: project 
---


This is an enhancement to the library website search function that allows librarians to highlight particular materials in the collection. 