---
id: 14
title: "Price List"
layout: project.njk 
img: "price-list"
path: "/projects/price-list/"
tags: project 
---


Various departments at the university are budgeted annual funds for purchase of new library materials. This application helps the library staff to track and manage these budgets as new items are acquired. 