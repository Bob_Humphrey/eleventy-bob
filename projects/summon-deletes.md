---
id: 21
title: "Summon Deletes"
layout: project.njk 
img: "summon-deletes"
path: "/projects/summon-deletes/"
tags: project 
---


Automated vendor notification of items removed from the library collection. 