---
id: 10
title: "Randall Library Staff"
layout: project.njk 
img: "staff"
path: "/projects/staff/"
tags: project 
---


An internal website used by library staff for various functions. The site houses a number of forms for initiating library processes. It is also used to gather input from library staff, especially when interviewing prospective library employees. 