---
id: 29
title: "Item Bib Mismatch"
layout: project.njk 
img: "item-bib-mismatch"
path: "/projects/item-bib-mismatch/"
tags: project 
---


An application that locates inconsistencies in the library catalog so that errors can be corrected. 