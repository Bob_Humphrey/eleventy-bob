---
id: 23
title: "Drupal 7 Migration"
layout: project.njk 
img: "drupal-7-migration"
path: "/projects/drupal-7-migration/"
tags: project 
---


Drupal is the library's open source content management system. I migrated 12 different websites from Drupal version 6 to Drupal version 7. 