---
id: 38
title: "Electronic Resources Tickets"
layout: project.njk 
img: "eresource-tickets"
path: "/projects/eresource-tickets/"
tags: project 
---



Ticketing app to manage problems with the library's electronic resources. 