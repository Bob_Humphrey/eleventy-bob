---
id: 20
title: "Batch Monitor"
layout: project.njk 
img: "batch-monitor"
path: "/projects/batch-monitor/"
tags: project 
---


The library has a number of computer jobs running automatically without anyone attending them. This application monitors these jobs and provides a report showing which jobs are running. It also notifies responsible personnel when any of the jobs fails or quits running. 