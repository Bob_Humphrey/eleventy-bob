const { src, dest, series, parallel } = require("gulp")
const chalk = require("chalk")
const Jimp = require("jimp")
const imageminWebp = require("imagemin-webp")
const through2 = require("through2")
const imagemin = require("gulp-imagemin")
const rename = require("gulp-rename")
const changed = require("gulp-changed")

const ASSETS_DIR = `../img`
const EXCLUDE_SRC_GLOB = `!(favicon*|*-256)`

/** resize images to input width
 * (only when images in "from" folder changed)
 * folder in "assets"
 * @param {string} from - input folder (file glob)
 * @param {string} to - output folder
 * @param {number} width - expect output width
 */
function resize(from, to, width) {
  const SRC = `${ASSETS_DIR}/${from}/**/${EXCLUDE_SRC_GLOB}*.{jpg,png}`
  const DEST = `${ASSETS_DIR}/${to}`

  console.log(
    chalk.bold("Resize from", chalk.underline(SRC), "to", chalk.underline(DEST))
  )

  return function resizeImage() {
    const quality = 100
    return src(SRC)
      .pipe(changed(DEST))
      .pipe(
        through2.obj(async function (file, _, cb) {
          if (file.isBuffer()) {
            const img = await Jimp.read(file.contents)
            const smallImg = img.resize(width, Jimp.AUTO).quality(quality)
            const content = await smallImg.getBufferAsync(Jimp.AUTO)

            file.contents = Buffer.from(content)
          }
          cb(null, file)
        })
      )
      .pipe(dest(DEST))
  }
}

/** convert images to webp
 * (only when images in "from" folder changed)
 * folder in "assets"
 * @param {string} from - input folder (file glob)
 * @param {string} to - output folder
 * @param {string} extension - optional, default is "webp"
 */
function convert(from, to, extension = "webp") {
  const SRC = `${ASSETS_DIR}/${from}/**/${EXCLUDE_SRC_GLOB}*.{jpg,png}`
  const DEST = `${ASSETS_DIR}/${to}`

  console.log(
    chalk.bold(
      "Convert from",
      chalk.underline(SRC),
      "to",
      chalk.underline(DEST)
    )
  )

  return function convertWebp() {
    return src(SRC)
      .pipe(changed(DEST, { extension: `.${extension}` }))
      .pipe(imagemin([imageminWebp({ quality: 80 })]))
      .pipe(
        rename({
          extname: `.${extension}`,
        })
      )
      .pipe(dest(DEST))
  }
}


exports.default = series(
  resize("originals/full", "resized/full/xl", 900),
  resize("originals/full", "resized/full/lg", 850),
  resize("originals/full", "resized/full/sm", 650),
  resize("originals/full", "resized/full/400", 400),
  resize("originals/full", "resized/full/300", 300),

  resize("originals/three-quarter", "resized/three-quarter/xl", 650),
  resize("originals/three-quarter", "resized/three-quarter/lg", 650),
  resize("originals/three-quarter", "resized/three-quarter/sm", 650),
  resize("originals/three-quarter", "resized/three-quarter/400", 400),
  resize("originals/three-quarter", "resized/three-quarter/300", 300),

  resize("originals/half", "resized/half/xl", 450),
  resize("originals/half", "resized/half/lg", 425),
  resize("originals/half", "resized/half/sm", 650),
  resize("originals/half", "resized/half/400", 400),
  resize("originals/half", "resized/half/300", 300),

  resize("originals/quarter", "resized/quarter/xl", 250),
  resize("originals/quarter", "resized/quarter/lg", 225),
  resize("originals/quarter", "resized/quarter/sm", 650),
  resize("originals/quarter", "resized/quarter/400", 400),
  resize("originals/quarter", "resized/quarter/300", 300),

  series(
    convert("resized/full/xl", "webp/full/xl"),
    convert("resized/full/lg", "webp/full/lg"),
    convert("resized/full/sm", "webp/full/sm"),
    convert("resized/full/400", "webp/full/400"),
    convert("resized/full/300", "webp/full/300"),

    convert("resized/three-quarter/xl", "webp/three-quarter/xl"),
    convert("resized/three-quarter/lg", "webp/three-quarter/lg"),
    convert("resized/three-quarter/sm", "webp/three-quarter/sm"),
    convert("resized/three-quarter/400", "webp/three-quarter/400"),
    convert("resized/three-quarter/300", "webp/three-quarter/300"),

    convert("resized/half/xl", "webp/half/xl"),
    convert("resized/half/lg", "webp/half/lg"),
    convert("resized/half/sm", "webp/half/sm"),
    convert("resized/half/400", "webp/half/400"),
    convert("resized/half/300", "webp/half/300"),

    convert("resized/quarter/xl", "webp/quarter/xl"),
    convert("resized/quarter/lg", "webp/quarter/lg"),
    convert("resized/quarter/sm", "webp/quarter/sm"),
    convert("resized/quarter/400", "webp/quarter/400"),
    convert("resized/quarter/300", "webp/quarter/300"),

  )
)
